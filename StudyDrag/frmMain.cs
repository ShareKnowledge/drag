﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudyDrag
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnDragUpload_Click(object sender, EventArgs e)
        {
            new frmDragUpload().ShowDialog();
        }

        private void btnLVToTV_Click(object sender, EventArgs e)
        {
            new frmDragListView().ShowDialog();
        }
    }
}
