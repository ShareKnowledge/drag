﻿namespace StudyDrag
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDragUpload = new System.Windows.Forms.Button();
            this.btnLVToTV = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDragUpload
            // 
            this.btnDragUpload.Location = new System.Drawing.Point(12, 12);
            this.btnDragUpload.Name = "btnDragUpload";
            this.btnDragUpload.Size = new System.Drawing.Size(123, 25);
            this.btnDragUpload.TabIndex = 0;
            this.btnDragUpload.Text = "拖拽上传";
            this.btnDragUpload.UseVisualStyleBackColor = true;
            this.btnDragUpload.Click += new System.EventHandler(this.btnDragUpload_Click);
            // 
            // btnLVToTV
            // 
            this.btnLVToTV.Location = new System.Drawing.Point(228, 14);
            this.btnLVToTV.Name = "btnLVToTV";
            this.btnLVToTV.Size = new System.Drawing.Size(200, 23);
            this.btnLVToTV.TabIndex = 1;
            this.btnLVToTV.Text = "ListView中的Item拖动到TreeView";
            this.btnLVToTV.UseVisualStyleBackColor = true;
            this.btnLVToTV.Click += new System.EventHandler(this.btnLVToTV_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 190);
            this.Controls.Add(this.btnLVToTV);
            this.Controls.Add(this.btnDragUpload);
            this.Name = "frmMain";
            this.Text = "Main Screen";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDragUpload;
        private System.Windows.Forms.Button btnLVToTV;
    }
}

