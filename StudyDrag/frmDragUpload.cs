﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudyDrag
{
    public partial class frmDragUpload : Form
    {
        public frmDragUpload()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 初始化属性
        /// </summary>
        void Init()
        {
            //设置为允许拖拽
            this.AllowDrop = true;
        }

        /// <summary>
        /// 注册事件
        /// </summary>
        void RegisteEvent()
        {
            this.DragEnter += FrmDragUpload_DragEnter;
            this.DragDrop += FrmDragUpload_DragDrop;
        }

        /// <summary>
        /// 当在此区域拖动落下的时候出发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmDragUpload_DragDrop(object sender, DragEventArgs e)
        {
            //获取拖动的数据
            IDataObject ido = e.Data;
            //如果拖动的数据是文件类型
            if (ido.GetDataPresent(DataFormats.FileDrop))
            {
                //获取文件的路径
                string[] paths = (string[])ido.GetData(DataFormats.FileDrop);

                //执行我们想要执行的业务逻辑，读取上传到服务器等等。
                foreach (var item in paths)
                {
                    Console.WriteLine(item);
                }
                MessageBox.Show("上传成功!");
            }
            else
            {
                MessageBox.Show("上传类型错误!");
            }
        }

        /// <summary>
        /// 当拖动到此区域的时候触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmDragUpload_DragEnter(object sender, DragEventArgs e)
        {
            //设置拖动的影响为：从拖动源复制到拖动的目标
            e.Effect = DragDropEffects.Copy;
        }

        /// <summary>
        /// 窗体初始化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmDragUpload_Load(object sender, EventArgs e)
        {
            this.Init();
            this.RegisteEvent();
        }
    }
}
