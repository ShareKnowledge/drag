﻿namespace StudyDrag
{
    partial class frmDragListView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvDep = new System.Windows.Forms.TreeView();
            this.lbDep = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // tvDep
            // 
            this.tvDep.Location = new System.Drawing.Point(247, 8);
            this.tvDep.Name = "tvDep";
            this.tvDep.Size = new System.Drawing.Size(223, 433);
            this.tvDep.TabIndex = 1;
            // 
            // lbDep
            // 
            this.lbDep.FormattingEnabled = true;
            this.lbDep.Location = new System.Drawing.Point(18, 8);
            this.lbDep.Name = "lbDep";
            this.lbDep.Size = new System.Drawing.Size(223, 433);
            this.lbDep.TabIndex = 2;
            this.lbDep.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbDep_MouseDown);
            // 
            // frmDragListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 448);
            this.Controls.Add(this.lbDep);
            this.Controls.Add(this.tvDep);
            this.Name = "frmDragListView";
            this.Text = "frmDragListView";
            this.Load += new System.EventHandler(this.frmDragListView_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TreeView tvDep;
        private System.Windows.Forms.ListBox lbDep;
    }
}